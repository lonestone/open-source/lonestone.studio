```
ERROR #85901  GRAPHQL
There was an error in your GraphQL query:
Unknown type "PrismicPageBodyText". Did you mean "SitePageContext"?
```

Vérifiez que le PRISMIC_ACCESS_TOKEN est bien renseigné dans le .env avec la bonne clé

--

```
ERROR #98124  WEBPACK
Generating SSR bundle failed
Can't resolve '@lonestone/ui-kit/dist/index' in 'C:\D\Sources\lonestone.studio\packages\front\src\components'****
```

Le front ne trouve pas la version build de l'ui-kit. Générer l'uikit avec

`cd packages/ui-kit && npm run build`

--
