<p align="center">
    <img alt="Gatsby" src="https://lonestone.consulting/images/brand-logo.png" width="300" />
</p>
<h1 align="center">
  Public website
</h1>

This project is a [Gatsby] static website with [Prismic] as data source and is deployed to [Netlify]. It use [ultra-runner] for monorepo management.

# 🚀 Getting started

Before running the project, you'll need to duplicate `.env.schema` to `.env` and ensure that the environnement variables are set.

You can find values here : [env]

```sh
# Install dependancies (don't use npm ; preinstall hook is not available, because render :/ )
pnpm i

# Build projects
npx ultra build
# or build only specific project
cd packages/xxx/ && npm run build

# Run front + storybook (ports 8000 / 6006 respectively)
# You have to build ui-kit to use front
npx ultra start

# Serve projects
npx ultra serve
```

# ⛏️ Built With

## Gatsby

Gatsby is a free and open source framework based on React that helps developers to build statics websites.

The project only has 2 pages in its folder, `404` & the `homepage`

The other pages are generated using the `page` content type from Prismic and the `createPages` hook in `gatsby-node.js`. Layout is defined is `/components/layout.tsx`

Each slice defined in Prismic have to have a corresponding React component in `src/components/slices`. Index between react component and prismic slide is in `src/components/slices/index.tsx`

## Prismic

The project uses [Prismic] as data source and will be redeployed every time a content is published.

_How to request Prismic with GraphQL ?_

All documents are pulled from repository and created as prismic {contentTypeName} and `allPrismic{contentTypeName}`, where {contentTypeName} is the API ID of your document’s content type.
For example, if you have Page as one of your content types :

`{ allPrismicPage { edges { node { [...] } } } }`

## Ultra

## Storybook

# 😃 Contribute

The commit conventions follow this pattern:

```
type(scope?): subject
body?
footer?
```

Type can be in `['build', 'ci', 'chore', 'docs', 'feat', 'fix', 'perf', 'refactor', 'revert', 'style', 'test']`

# 💎 Code Quality

The codebase is covered by react + typescript generals recommandations and sonarjs. The first one will analyse code rules
while sonar will analyse code to detect bugs and suspicious patterns. See more in npm [sonarjs]

## Common mistakes :

**Cognitive Complexity**

_`Refactor this function to reduce its Cognitive Complexity`_

This means your function is really complexe with lot of if, else, etc.. You need to refactor it to make it easy to understand

# 🚓 License

Copyright Lonestone

[gatsby]: https://www.gatsbyjs.org/
[prismic]: https://lonestone-studio.prismic.io/
[netlify]: https://app.netlify.com/sites/lonestone/
[env]: https://www.notion.so/lonestone/Config-env-1e7bade8b16c423982bbb00c0ee1ac3d
[ultra-runner]: https://www.npmjs.com/package/ultra-runner
[sonarjs]: https://github.com/SonarSource/eslint-plugin-sonarjs
