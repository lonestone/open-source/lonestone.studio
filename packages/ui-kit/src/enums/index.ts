export enum Theme {
  DEFAULT = 'default',
  PRIMARY = 'primary',
}

export enum Size {
  SMALL = 'small',
  MEDIUM = 'default',
  LARGE = 'large',
}
