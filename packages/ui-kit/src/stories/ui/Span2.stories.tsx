import * as React from 'react'
import { Button } from '@storybook/react/demo'
import Span2 from '../../ui/Span2'

export default {
  title: 'UI|Button',
  component: Button,
}

export const Basic = () => <Span2 txt="hello" />

export const Test = () => <Span2 txt="Test" />
