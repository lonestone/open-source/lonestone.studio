import React from 'react'

interface Props {
  txt: string
}

const Span2: React.FC<Props> = ({ txt, ...restProps }) => {
  return <span {...restProps}>Custom span : {txt}</span>
}

export default Span2
