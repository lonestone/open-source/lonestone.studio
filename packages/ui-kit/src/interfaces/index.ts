export interface Color {
  id: string
  name: string
  hexa: string
  index: number
}
