require('dotenv').config()

const website = require('./config/website')
const { linkResolver } = require('./src/utils/gatsby-node-helpers')

module.exports = {
  pathPrefix: website.pathPrefix,
  siteMetadata: {
    title: website.title,
    description: website.description,
    siteUrl: website.url,
    banner: website.logo,
    ogLanguage: website.ogLanguage,
    author: website.author,
    twitter: website.twitter,
    facebook: website.facebook,
  },
  plugins: [
    // TODO : enable to keep planet when we navigate on website
    // {
    //   resolve: `gatsby-plugin-layout`,
    //   options: {
    //     component: require.resolve(`./src/templates/page.tsx`),
    //   },
    // },
    {
      resolve: 'gatsby-plugin-sentry',
      options: {
        dsn: process.env.SENTRY_DSN,
        // Optional settings, see https://docs.sentry.io/clients/node/config/#optional-settings
        environment: process.env.NODE_ENV,
        release: process.env.RELEASE,
        enabled: ['production', 'staging'].includes(process.env.NODE_ENV),
      },
    },
    {
      resolve: 'gatsby-source-prismic',
      options: {
        repositoryName: 'lonestone-studio',
        accessToken: `${process.env.PRISMIC_ACCESS_TOKEN}`,
        linkResolver,
      },
    },
    'gatsby-plugin-typescript',
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/images`,
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    'gatsby-plugin-pnpm',
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: website.title,
        short_name: website.titleAlt,
        start_url: '/',
        description: website.description,
        background_color: website.backgroundColor,
        theme_color: website.themeColor,
        display: 'standalone',
        icon: website.favicon,
      },
    },
  ],
}
