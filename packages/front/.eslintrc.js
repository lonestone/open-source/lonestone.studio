require('eslint-plugin-resolution-patch')

module.exports = {
  extends: [
    'plugin:react/recommended', // Uses the recommended rules from @eslint-plugin-react
    'plugin:sonarjs/recommended',
    'plugin:security/recommended',
    'eslint-config-lonestone-typescript', // General typescript rules
  ],
  settings: {
    react: {
      version: 'detect', // Tells eslint-plugin-react to automatically detect the version of React to use
    },
  },
  rules: {
    'react/prop-types': 'off',
  },
  plugins: ['sonarjs', 'security'],
}
