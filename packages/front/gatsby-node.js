const locales = require('./config/i18n')
const {
  replaceTrailing,
  replaceBoth,
} = require('./src/utils/gatsby-node-helpers')

// Take the pages from src/pages and generate pages for all locales, e.g. /index and /en/index
exports.onCreatePage = ({ page, actions }) => {
  const { createPage, deletePage } = actions

  // Only create one 404 page at /404.html
  // if (page.path.includes('404')) {
  //   return
  // }

  // First delete the pages so we can re-create them
  deletePage(page)

  Object.keys(locales).map(locale => {
    // Remove the trailing slash from the path, e.g. --> /categories
    page.path = replaceTrailing(page.path)

    // Remove the leading AND trailing slash from path, e.g. --> categories
    const name = replaceBoth(page.path)

    // Create the "slugs" for the pages. Unless default language, add prefix like "/en"
    const localizedPath = locales[locale].isDefault
      ? page.path
      : `${locales[locale].path}${page.path}`

    return createPage({
      ...page,
      path: localizedPath,
      context: {
        locale,
        name,
      },
    })
  })
}

exports.createPages = async ({ actions, graphql, reporter }) => {
  const { createPage } = actions

  const result = await graphql(`
    query {
      pages: allPrismicPage {
        edges {
          node {
            uid
            lang
          }
        }
      }
    }
  `)
  if (result.errors) {
    reporter.panicOnBuild('🚨  ERROR: Loading "createPages" query')
  }

  result.data.pages.edges.forEach(page => {
    const { uid, lang } = page.node

    const path = locales[lang].isDefault ? uid : `${locales[lang].path}/${uid}`

    createPage({
      path,
      component: require.resolve('./src/templates/page.tsx'),
      context: {
        uid: uid,
        locale: lang,
        name: uid,
      },
    })
  })
}

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    module: {
      rules: [
        {
          test: /\.fbx$/,
          use: [`file-loader`],
        },
      ],
    },
  })
}
