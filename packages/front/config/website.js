module.exports = {
  pathPrefix: '/',
  title: 'Lone Stone',
  titleAlt: 'Lone Stone',
  description: 'Description',
  author: '@lonestone',
  headline: 'Headline for schema.org JSONLD',
  url: 'https://lonestone.studio',
  ogLanguage: 'fr_FR',
  logo: 'src/images/favicon.png',
  favicon: 'src/images/favicon.png',
  shortName: 'Lone Stone',
  themeColor: '#c74c22',
  backgroundColor: '#c74c22',

  twitter: '@LoneStoneStudio',
  // facebook: '',
  // googleAnalyticsID: 'UA-XXXXXX-X',
}
