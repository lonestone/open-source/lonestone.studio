/**
 * Gatsby helpers
 */
const i18n = require('../../config/i18n')

// Remove trailing slashes unless it's only "/", then leave it as it is
exports.replaceTrailing = path =>
  path === `/` ? path : path.replace(/\/$/, ``)

// Remove slashes at the beginning and end
exports.replaceBoth = path => path.replace(/^\/|\/$/g, '')

exports.linkResolver = doc => {
  const prefix =
    !doc.lang || i18n[doc.lang].isDefault ? `/` : `/${i18n[doc.lang].path}/`

  return `${prefix}${doc.uid}`
}
