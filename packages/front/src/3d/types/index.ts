import { Group, AnimationClip } from 'three'

export interface GroupWithAnimation extends Group {
  animations?: Array<AnimationClip>
}
