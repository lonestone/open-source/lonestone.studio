import React, { useMemo } from 'react'
import FBXModel, { FBXModelProps } from './loaders/FBXLoader'
// TODO remove that
// eslint-disable-next-line @typescript-eslint/no-var-requires
const JumpingCubeModel = require('../assets/JumpingCube.fbx')

export type JumpingCubeProps = Omit<FBXModelProps, 'url' | 'animation'>

interface OwnProps {
  action?: 'jump'
}

type Props = OwnProps & JumpingCubeProps

const JumpingCube: React.FC<Props> = ({ action, ...props }) => {
  const animation = useMemo(() => {
    // eslint-disable-next-line sonarjs/no-small-switch
    switch (action) {
      case 'jump':
        return 'Take 001'
      default:
        return ''
    }
  }, [action])

  return FBXModel({
    url: JumpingCubeModel,
    animation,
    ...props,
  })
}

export default JumpingCube
