import React from 'react'

const Sphere: React.FC = ({ visible = true, ...props }) => {
  return (
    <mesh {...props} visible={visible}>
      <icosahedronBufferGeometry attach="geometry" args={[1, 1]} />
      <meshStandardMaterial attach="material" color={'green'} />
    </mesh>
  )
}

export default Sphere
