import React, { useRef } from 'react'
import { useFrame } from 'react-three-fiber'

const ExampleCube: React.FC = ({ ...props }) => {
  const mesh = useRef()

  useFrame(() => (mesh.current.rotation.x = mesh.current.rotation.y += 0.01))

  return (
    <mesh {...props} ref={mesh}>
      <boxBufferGeometry attach="geometry" args={[1, 1, 1]} />
      <meshStandardMaterial attach="material" color="orange" />
    </mesh>
  )
}

export default ExampleCube
